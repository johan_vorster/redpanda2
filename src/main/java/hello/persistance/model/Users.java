package hello.persistance.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Users", uniqueConstraints = {@UniqueConstraint(columnNames = "Username")})
public class Users implements Serializable {
    private static final long serialVersionUID = -1798070786993154676L;
    @Id
    @Column(name = "Username", unique = true, nullable = false)
    private String username;
    @Column(name = "Password", unique = false, nullable = false, length = 100)
    private String password;
   @Column(name = "HappyCustomers", unique = false, nullable = false, length = 100)
    private int happyCustomers;



    private boolean currentlyActive;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getHappyCustomers() {
        return happyCustomers;
    }

    public void setHappyCustomers(int happyCustomers) {
        this.happyCustomers = happyCustomers;
    }
    public boolean isCurrentlyActive() {
        return currentlyActive;
    }

    public void setCurrentlyActive(boolean currentlyActive) {
        this.currentlyActive = currentlyActive;
    }


}
