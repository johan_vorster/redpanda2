package hello;

import hello.persistance.model.HibernateUtil;
import hello.persistance.model.Users;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class GreetingController {
    Session session = HibernateUtil.getSessionFactory().openSession();
    String currentUser = "";
    @GetMapping("/greeting")
    public String greetingForm(Model model) {
        model.addAttribute("users", new Users());
        return "greeting";
    }

    @GetMapping("/happyButton")
    public String happyButton(Model model) {
        model.addAttribute("users", new Users());
        return "result";
    }

    @PostMapping("/greeting")
    public String greetingSubmit(@ModelAttribute Users users) {
        List results = session.createQuery("select u from Users u where u.username = :username")
                .setParameter("username", users.getUsername())
                .list();
        if(results.size()>0)
        {
            Iterator<?> a = results.iterator();
            Users b= (Users)a.next();
            if(users.getPassword() == b.getPassword())
            {
                return "result";
            }else
            {
                return "error";
            }
        }else
        {
            session.beginTransaction();
            // Add new Employee object
            Users user = new Users();
            user.setUsername(users.getUsername());
            user.setPassword(users.getPassword());
            user.setHappyCustomers(0);
            currentUser = users.getUsername();
            session.save(user);
            session.getTransaction().commit();
            //   session.close();
            return "result";
        }
    }

    @PostMapping("/happyButton")
    public String happyCustomer() {

        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Users user = (Users)session.get(Users.class, currentUser);
            int happyCustomers = user.getHappyCustomers();
            happyCustomers = happyCustomers+1;
            user.setHappyCustomers(happyCustomers);
            session.update(user);
            tx.commit();
            session.flush();
            session.evict(user);
        }catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        }finally {
            //session.close();
        }
        return "result";
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String messages(Model model) {
        System.out.println(session.createQuery("from Users").list());
        List<?> umh = session.createQuery("from Users").list();
        Iterator<?> umhI = umh.iterator();
        while (umhI.hasNext())
        {
            Users a = (Users)umhI.next();;
        }
        model.addAttribute("list",session.createQuery("from Users").list());
        return "list";
    }
}